package com.apuntesdejava.sakila.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@ApplicationPath("/api")
public class ApplicationConfiguration extends Application {

}
