package com.apuntesdejava.sakila.rest;

import com.apuntesdejava.sakila.domain.Actor;
import com.apuntesdejava.sakila.repository.ActorRepository;
import com.apuntesdejava.sakila.request.ActorRequest;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@Path("actor")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@ApplicationScoped
public class ActorREST {

    /**
     * Repositorio de Actor
     */
    @Inject
    private ActorRepository actorRepository;

    @GET
    public Response findAll(
            @QueryParam("limit") @DefaultValue("100") int limit,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("hint") @DefaultValue("") String hint) {
        List<Actor> list = actorRepository.findRange(start, limit, hint);
        return Response.ok(list).build();
    }

    @GET
    @Path("{actorId}")
    public Response findById(
            @PathParam("actorId") long actorId) {
        Actor actor = actorRepository.findById(actorId);
        return Response.ok(actor).build();
    }

    @GET
    @Path("{actorId}/films")
    public Response actorFilms(
            @PathParam("actorId") long actorId) {
        Actor actor = actorRepository.findById(actorId); //busca...
        if (actor == null) { //... si no lo encuentra..
            return Response.status(Response.Status.NOT_FOUND) //.. le dice que no existe
                    .build();
        }
        //y si lo encuentra, le devuelve la lista.
        return Response.ok(actor.getFilms()).build();
    }

    @POST
    public Response create(ActorRequest request) {
        Actor actor = new Actor(); //una instancia de la entidad
        actor.setFirstName(request.getFirstName()); //guardamos nombre..
        actor.setLastName(request.getLastName()); //... apellido..
        actor = actorRepository.create(actor); //... y lo guardamos en el repo
        return Response.ok(actor).build(); //devolvemos el objeto creado
    }

    @PUT
    @Path("{actorId}") //el ID via URI
    public Response update(
            @PathParam("actorId") long actorId, //... el ID identificado
            ActorRequest request //... y el JSON
    ) {
        Actor actor = actorRepository.findById(actorId); //buscamos por el ID
        if (actor == null) { // si no lo encuentra...
            return Response.status(Response.Status.NOT_FOUND).build(); //.. le avisa
        }
        actor.setFirstName(request.getFirstName()); //guardamos nombre..
        actor.setLastName(request.getLastName());   //... apellido
        actor = actorRepository.update(actor);      //... actualizamos via repo
        return Response.ok(actor).build();          //... y devolvemos el objeto

    }
}
