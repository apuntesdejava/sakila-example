package com.apuntesdejava.sakila.repository;

import com.apuntesdejava.sakila.domain.Actor;
import java.time.LocalDateTime;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@ApplicationScoped
public class ActorRepository {

    @Inject
    private EntityManager em;

    public List<Actor> findRange(int start, int count, String hint) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Actor> cq = cb.createQuery(Actor.class);
        Root<Actor> actor = cq.from(Actor.class);
        if (hint != null && !hint.isEmpty()) {
            hint = "%" + hint.replace(' ', '%') + "%"; //comodines
            cq.where(
                    cb.or(
                            cb.like(actor.get("firstName"), hint),
                            cb.like(actor.get("lastName"), hint)
                    )
            );
        }
        TypedQuery<Actor> query = em.createQuery(cq)
                .setMaxResults(count)
                .setFirstResult(start);
        return query.getResultList();

    }

    public List<Actor> findRange(int start, int count) {
        return em.createQuery("select a from Actor a", Actor.class)
                .setMaxResults(count)
                .setFirstResult(start)
                .getResultList();
    }

    public Actor findById(long actorId) {
        return em.find(Actor.class, actorId);
    }

    @Transactional
    public Actor create(Actor actor) {
        actor.setLastUpdate(LocalDateTime.now());
        em.persist(actor);
        return actor;
    }

    @Transactional
    public Actor update(Actor actor) {
        em.find(Actor.class, actor.getActorId());
        actor.setLastUpdate(LocalDateTime.now());
        return em.merge(actor);
    }
}
