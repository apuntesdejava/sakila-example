package com.apuntesdejava.sakila.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@ApplicationScoped
public class JPAProducer {

    @PersistenceContext
    @Produces
    private EntityManager em;

}
