package com.apuntesdejava.sakila.domain.converter;

import java.time.LocalDate;
import java.sql.Date;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@Converter(autoApply = true)
public class LocalDateConverter
        implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate value) {
        return value == null ? null : Date.valueOf(value);
    }

    @Override
    public LocalDate convertToEntityAttribute(Date value) {
        return value == null 
                ? null
                : new java.sql.Date(value.getTime()).toLocalDate();
    }

}

