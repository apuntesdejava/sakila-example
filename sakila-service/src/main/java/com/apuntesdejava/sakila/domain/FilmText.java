package com.apuntesdejava.sakila.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
@Entity
@Table(name = "film_text")
public class FilmText implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "film_id")
    private Long filmId;

    @Column(name = "title")
    private String title;
    
    @Column(name = "description")
    private String description;

    public Long getFilmId() {
        return filmId;
    }

    public void setFilmId(Long filmId) {
        this.filmId = filmId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (filmId != null ? filmId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FilmText)) {
            return false;
        }
        FilmText other = (FilmText) object;
        return !((this.filmId == null && other.filmId != null) || (this.filmId != null && !this.filmId.equals(other.filmId)));
    }

    @Override
    public String toString() {
        return "com.apuntesdejava.sakila.domain.FilmText[ id=" + filmId + " ]";
    }

}
