package com.apuntesdejava.sakila.request;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
public class ActorRequest {

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
