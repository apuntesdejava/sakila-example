/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your customer ViewModel code goes here
 */
const ACTOR_URI = "http://localhost:8080/sakila-service/api/actor/";

define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojarraydataprovider',
    'ojs/ojlistview',
    'ojs/ojbutton',
    'ojs/ojlabel',
    'ojs/ojinputtext',
    'ojs/ojformlayout'],
        function (oj, ko, $, ArrayDataProvider) {

            function CustomerViewModel() {
                var self = this;
                // Below are a set of the ViewModel methods invoked by the oj-module component.
                // Please reference the oj-module jsDoc for additional information.

                self.actors = ko.observableArray();
                self.dataProvider = new ArrayDataProvider(
                        self.actors,
                        {'keyAttributes': 'actorId'}
                );


                /**
                 * Carga todos los registros
                 * @return {undefined}
                 */
                self.loadActorList = function () {
                    $.getJSON(ACTOR_URI, (data) => {
                        self.actors(data);
                    });
                };



                /**
                 * intercambia las ventanas de listado y formulario
                 * @return {undefined}
                 */
                self.slide = function () {
                    $("#listPage,#formPage").toggle();

                };
                /**
                 * Formulario
                 */
                self.form = {
                    actorId: ko.observable(0),
                    firstName: ko.observable(),
                    lastName: ko.observable("")
                };
                /**
                 * Prepara el formulario para un nuevo registro
                 * @return {undefined}
                 */
                self.newActor = function () {
                    self.slide();
                    self.form
                            .actorId(0)
                            .firstName("")
                            .lastName("");
                };
                /**
                 * Cierra la ventana del formulario
                 * @return {undefined}
                 */
                self.formCancel = function () {
                    self.slide();
                };
                /**
                 * Guarda los datos del formulario. 
                 * Si es nuevo (actorId===0), lo envia como POST
                 * Sino, es una actualizacion y lo envia como PUT
                 * @return {undefined}
                 */
                self.formSave = function () {
                    var actorForm = {
                        firstName: self.form.firstName(),
                        lastName: self.form.lastName()
                    };
                    var jsonForm = JSON.stringify(actorForm); //form en json
                    var actorId = self.form.actorId();
                    var isNew = actorId === 0; //si es 0 es true
                    $.ajax(ACTOR_URI + (isNew ? "" : actorId), {
                        method: isNew ? 'post' : 'put', //POST para nuevo
                        data: jsonForm,
                        contentType: 'application/json',
                        success: (data) => {
                            self.loadActorList(); //carga el listado
                            self.slide(); //cambia de ventanas
                        }
                    });
                };
                /**
                 * Carga los datos del actor seleccionado de la lista
                 * @param {type} event
                 * @return {undefined}
                 */
                self.loadActor = function (event) {
                    var actorId = event.detail.value; //id del elemento en el ListView
                    console.log("actorId:", actorId);
                    //obtiene el registro usando el ID en el parámetro
                    $.getJSON(ACTOR_URI + actorId, (data) => {
                        console.log("data:", data); //en data está el registro...
                        self.form.actorId(data.actorId) //... y pone los campos en cada campo del formulario
                                .firstName(data.firstName)
                                .lastName(data.lastName);
                        self.slide(); //intercambia las ventanas
                    });
                };
                self.handleRawValueChanged = function (evt) {
                    var value = evt.detail.value;
                    if (value === null)
                        value = "";
                    $.getJSON(ACTOR_URI + '?hint=' + value, (data) => {
                        self.actors(data);
                    });
                };


                /**
                 * Optional ViewModel method invoked after the View is inserted into the
                 * document DOM.  The application can put logic that requires the DOM being
                 * attached here. 
                 * This method might be called multiple times - after the View is created 
                 * and inserted into the DOM and after the View is reconnected 
                 * after being disconnected.
                 */
                self.connected = function () {
                    console.log("connected");
                    // Implement if needed
                };

                /**
                 * Optional ViewModel method invoked after the View is disconnected from the DOM.
                 */
                self.disconnected = function () {
                    console.log("disconnected");
                    // Implement if needed
                };

                /**
                 * Optional ViewModel method invoked after transition to the new View is complete.
                 * That includes any possible animation between the old and the new View.
                 */
                self.transitionCompleted = function () {
                    console.log("transitionCompleted");
                    self.loadActorList();
                    // Implement if needed
                };
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constructed
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new CustomerViewModel();
        }
);
